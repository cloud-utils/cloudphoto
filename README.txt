Cloudphoto

Usage:
cloudphoto -h

Settings:
1. From env variables:
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
AWS_REGION_NAME
AWS_BUCKET_NAME
AWS_ENDPOINT_URL

2. From ini files:
Use `-c` option to set filepath with desired section (ex: config.ini.default)
