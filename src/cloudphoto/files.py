import os
import re
from typing import Optional

import aioboto3
import aiofiles
import attr
from cloudphoto.exceptions import AlbumNotFound
from cloudphoto.exceptions import DirectoryNotFound
from cloudphoto.settings import Settings


@attr.define
class FileManager:
    settings: Settings
    session: aioboto3.Session = attr.ib(init=False)
    delimiter: str = '/'

    def __attrs_post_init__(self):
        self.session = aioboto3.Session(
            aws_access_key_id=self.settings.aws_access_key_id,
            aws_secret_access_key=self.settings.aws_secret_access_key,
            region_name=self.settings.aws_region_name,
        )

    def _check_dir(self, src_dir: str):
        if not os.path.isdir(src_dir):
            raise DirectoryNotFound(f'Directory "{src_dir}" not found.')

    async def _check_album(self, album: str):
        async with self.session.client(
            's3',
            endpoint_url=self.settings.aws_endpoint_url,
        ) as s3client:
            prefix = album + self.delimiter
            response = await s3client.list_objects_v2(
                Bucket=self.settings.aws_bucket_name,
                Prefix=prefix,
                MaxKeys=1,
            )
            if not response.get('Contents'):
                raise AlbumNotFound(f'Album {album} not found')

    async def upload(self, src_dir: str, album: str):
        self._check_dir(src_dir)
        files = []
        for f in os.listdir(src_dir):
            if re.search(r'.jpe?g$', f) and os.path.isfile(
                os.path.join(src_dir, f),
            ):
                files.append(f)
        async with self.session.client(
            's3',
            endpoint_url=self.settings.aws_endpoint_url,
        ) as s3client:
            for file in files:
                body = await aiofiles.open(os.path.join(src_dir, file), 'rb')
                key = album + self.delimiter + file
                await s3client.upload_fileobj(
                    body,
                    self.settings.aws_bucket_name,
                    key,
                )

    async def download(self, src_dir: str, album: str):
        self._check_dir(src_dir)
        await self._check_album(album)
        async with self.session.resource(
            's3',
            endpoint_url=self.settings.aws_endpoint_url,
        ) as s3resource:
            bucket = await s3resource.Bucket(self.settings.aws_bucket_name)
            async for s3object in bucket.objects.filter(Prefix=album):
                filename = s3object.key.split(self.delimiter)[-1]
                dest = os.path.join(src_dir, filename)
                await bucket.download_file(s3object.key, dest)

    async def list(self, album: Optional[str]):
        prefix = album or ''
        files = []
        async with self.session.resource(
            's3',
            endpoint_url=self.settings.aws_endpoint_url,
        ) as s3resource:
            bucket = await s3resource.Bucket(self.settings.aws_bucket_name)
            async for s3object in bucket.objects.filter(Prefix=prefix):
                album_name, filename = s3object.key.split(self.delimiter)
                files.append(filename if album else album_name)
        print(files)
