import argparse
import asyncio
from typing import Optional
from typing import Sequence

from cloudphoto.files import FileManager
from cloudphoto.settings import Settings


async def async_main(argv: Optional[Sequence[str]] = None):
    parser = argparse.ArgumentParser()
    parser.add_argument('action', choices=['upload', 'download', 'list'])

    parser.add_argument(
        '-p',
        dest='path',
        type=str,
    )
    parser.add_argument(
        '-a',
        dest='album',
        type=str,
    )
    parser.add_argument(
        '-c',
        dest='config',
    )

    args = parser.parse_args(argv)

    if args.config:
        settings = Settings.from_file(args.config)
    else:
        settings = Settings.from_env()
    manager = FileManager(settings)

    if args.action == 'upload':
        await manager.upload(args.path, args.album)
    elif args.action == 'download':
        await manager.download(args.path, args.album)
    elif args.action == 'list':
        await manager.list(args.album)


def main():
    asyncio.run(async_main())


if __name__ == '__main__':
    exit(main())
