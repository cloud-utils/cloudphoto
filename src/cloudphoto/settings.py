import configparser
import os

from attr import define


@define
class Settings:
    aws_access_key_id: str
    aws_secret_access_key: str
    aws_region_name: str
    aws_bucket_name: str
    aws_endpoint_url: str

    @classmethod
    def from_env(cls) -> 'Settings':
        return Settings(
            aws_access_key_id=os.environ['AWS_ACCESS_KEY_ID'],
            aws_secret_access_key=os.environ['AWS_SECRET_ACCESS_KEY'],
            aws_region_name=os.environ['AWS_REGION_NAME'],
            aws_bucket_name=os.environ['AWS_BUCKET_NAME'],
            aws_endpoint_url=os.environ['AWS_ENDPOINT_URL'],
        )

    @classmethod
    def from_file(cls, config: str) -> 'Settings':
        filename, section = config.rsplit('.', 1)
        parser = configparser.ConfigParser()
        parser.read(filename)
        return Settings(**parser[section])
